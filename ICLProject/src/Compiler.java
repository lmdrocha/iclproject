import ast.ASTNode;
import compiler.CodeBlock;
import compiler.CompEnvironment;
import parser.Parser;

public class Compiler {

	public static void main(String[] args) throws Exception {
		Parser parser = new Parser(System.in);
	    ASTNode exp;

	    while (true) {
	    try {
	    exp = parser.Start();
	    CompEnvironment env = new CompEnvironment();
        CodeBlock code = new CodeBlock("file");
	    exp.comp(env, code);
        code.writeCode("compiled");
	    } catch (Exception e) {
	      System.out.println ("Syntax Error!");
	      e.printStackTrace();
	      parser.ReInit(System.in);
	    }
	    }
	  }
}
