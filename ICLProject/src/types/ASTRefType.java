package types;

public class ASTRefType implements IType {
	private IType subType;
	
	public ASTRefType(IType subType) {
        this.subType = subType;
    }
	
	public IType subType() {
        return subType;
    }

    public String primitive() {
        return "Ljava/lang/Object;";
    }
    
    @Override
    public boolean equals(Object o) {
    	ASTRefType objectRef;

        if(!(o instanceof ASTRefType)) {
            return false;
        }

        objectRef = (ASTRefType) o;

        return subType.equals(objectRef.subType());
    }
    
    @Override
    public String toString() {
        return "ref(" + subType.toString() + ")";
    }

}
