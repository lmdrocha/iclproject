package types;

public class ASTBoolType implements IType {
	public static ASTBoolType singleton = new ASTBoolType();

    public String primitive() {
        return "I";
    }

    @Override
    public String toString() {
        return "bool";
    }

}
