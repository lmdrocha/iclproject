package types;

import java.util.Iterator;
import java.util.List;

public class ASTFunType implements IType {
	private List<IType> argumentsTypes;
    private IType resultType;
	
	public ASTFunType(List<IType> argumentsTypes, IType resultType) {
        this.argumentsTypes = argumentsTypes;
        this.resultType = resultType;
    }

	public List<IType> argumentsTypes() {
        return argumentsTypes;
    }

    public IType resultType() {
        return resultType;
    }
	
	public String primitive() {
        return "Ljava/lang/Object;";
    }
	
	@Override
    public boolean equals(Object o) {
		ASTFunType objectFun;
        Iterator<IType> itThis;
        Iterator<IType> itObj;

        if(!(o instanceof ASTFunType)) {
            return false;
        }

        objectFun = (ASTFunType) o;

        if(argumentsTypes.size() != objectFun.argumentsTypes().size()) {
            return false;
        }

        itThis = argumentsTypes.iterator();
        itObj = objectFun.argumentsTypes.iterator();

        while(itThis.hasNext() && itObj.hasNext()) {
            if(!itThis.next().equals(itObj.next())) {
                return false;
            }
        }

        return resultType.equals(objectFun.resultType());
    }

	@Override
    public String toString() {
        String strArgs, strFinal;
        Iterator<IType> it;

        strArgs = "";
        it = argumentsTypes.iterator();

        while(it.hasNext()) {
            IType next = it.next();

            strArgs += next.toString() + ", ";
        }

        strArgs = strArgs.substring(0, strArgs.length() - 2);
        strFinal = "fun(" + strArgs + ")" + resultType.toString();

        return strFinal;
    }

}
