package types;

public class ASTIntType implements IType {
	public static ASTIntType singleton = new ASTIntType();

    public String primitive() {
        return "I";
    }

    @Override
    public String toString() {
        return "int";
    }

}
