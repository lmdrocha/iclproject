import ast.ASTNode;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import parser.Parser;
import types.IType;
import objects.IValue;

public class Main {
	  /** Main entry point. */
	  public static void main(String args[]) {
	    Parser parser = new Parser(System.in);
	    ASTNode exp;

	    while (true) {
	    try {
	    exp = parser.Start();
	    Environment<IType> envT  = new Environment<>();
	    Environment<IValue> envV= new Environment<>();
	    IType type = exp.typecheck(envT);
	    IValue value = exp.eval(envV);
	    String str = "Result: "+ value.toString() + " [Type: " + type.toString() + "] ";

        System.out.println(str);
	    } catch (TypeErrorException ex) {
            System.err.println("Typecheck Error: " + ex.getMessage());
        } catch (UndeclaredIdentifierException ex) {
            System.err.println("Typecheck Error: " + ex.getMessage());
        } catch (Exception e) {
	      System.out.println ("Syntax Error!");
	      e.printStackTrace();
	      parser.ReInit(System.in);
	    }
	    }
	  }
}
