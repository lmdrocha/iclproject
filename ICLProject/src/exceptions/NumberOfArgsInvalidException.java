package exceptions;

public class NumberOfArgsInvalidException extends Exception{

	private static final long serialVersionUID = 7038919712343920L;
	
	private String message;
	
	public NumberOfArgsInvalidException(String message) {
		this.message=message;
	}
	
	public String getMessage() {
		return message;
	}

}
