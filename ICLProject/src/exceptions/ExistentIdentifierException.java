package exceptions;

public class ExistentIdentifierException extends Exception{

	private static final long serialVersionUID = -8599778058543272654L;

	private String identifier;

	public ExistentIdentifierException(String identifier) {
		this.identifier=identifier;
	}

	public String getMessage() {
		return "Identifier with value: '"+identifier+"' already exists";
	}
	
}
