package exceptions;

public class UndeclaredIdentifierException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4121974517108700951L;

	private String identifier;

	public UndeclaredIdentifierException(String identifier) {
		this.identifier=identifier;
	}

	public String getMessage() {
		return "Undeclared identifier with id:"+identifier;
	}
}
