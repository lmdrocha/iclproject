package exceptions;

public class TypeErrorException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7038919716263323920L;
	
	private String message;
	
	public TypeErrorException(String message) {
		this.message=message;
	}
	
	public String getMessage() {
		return message;
	}

}
