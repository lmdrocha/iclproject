package objects;

import ast.ASTNode;
import types.IType;

public class Binding {
	
	private String id;
	private ASTNode expression;
	private IType type;

	public Binding(String id, ASTNode expression,IType type){
		this.id=id;
		this.expression=expression;
		this.type=type;
	}
	
	public String getID(){
		return id;
	}
	
	public ASTNode getExpression(){
		return expression;
	}

	public IType getType() {
		return type;
	}

}
