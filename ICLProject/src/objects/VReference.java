package objects;

public class VReference implements IValue {
	private IValue val;
	
	public VReference (IValue value) {
		this.val = value;
	}
	
	public IValue getValue() {
		return val;
	}
	
	public void setValue(IValue value) {
		this.val = value;
	}

	public IValue get() {
		return val;
	}

}
