package objects;

public class VInt implements IValue {

	private int v;

	public VInt(int v0) {
		v = v0;
	}

	public int getValue() {
		return v;
	}

	@Override
	public String toString() {
		return String.valueOf(v);
	}
}
