package objects;

import types.IType;

public class IdType {
	private String id;
	private IType type;

	public IdType(String id, IType type) {
		this.id = id;
		this.type = type;
	}

	public String id() {
		return id;
	}

	public IType type() {
		return type;
	}
}
