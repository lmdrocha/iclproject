package objects;

import java.util.List;

import ast.ASTNode;

public class VFun implements IValue {
	
	private List<IdType> typelist;
    private ASTNode body;
    private Environment env;

    public VFun(List<IdType> typelist, ASTNode body, Environment env) {
        this.typelist = typelist;
        this.body = body;
        this.env = env;
    }

    public List<IdType> typelist() {
        return typelist;
    }

    public ASTNode body() {
        return body;
    }

    public Environment env() {
        return env;
    }

}
