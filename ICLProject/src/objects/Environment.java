package objects;

import java.util.Map;
import java.util.TreeMap;

public class Environment<T> {
	Environment<T> previous;
	Map<String, T> associateds;
	
	public Environment(){
		this.previous=null;
		this.associateds=new TreeMap<>();
	}
	
	public Environment(Environment up){
		this.previous=up;
		this.associateds=new TreeMap<>();
	}

	public Environment beginScope() {
		return new Environment(this);
		
	}
	
	public Environment endScope() {
		return previous;
		
	}
	
	public T find(String id){
		T val = associateds.get(id);
		if(val == null && previous != null) {
			val = previous.find(id);
		}
		return val;
		
	}
	
	public void assoc(String id,T value){
		associateds.put(id, value);
	}
	
}
