package objects;

public class VBoolean implements IValue{
	
	private boolean bool;
	
	public VBoolean(boolean bool) {
		this.bool = bool;
	}
	
	public boolean getValue() {
		return bool;
	}
	
	@Override
	public String toString() {
		return String.valueOf(bool);
	}

}
