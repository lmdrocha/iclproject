package compiler;

public class Field {
    private String id;
    private String name;
    private String type;

    public Field(String id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public String id() {
        return id;
    }

    public String name() {
        return name;
    }

    public String type() {
        return type;
    }
}
