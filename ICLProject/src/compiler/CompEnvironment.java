
package compiler;

import java.util.ArrayList;

import exceptions.ExistentIdentifierException;
import exceptions.UndeclaredIdentifierException;

public class CompEnvironment {

	
	CompEnvironment previous;
	ArrayList<Field> fields;
	int fieldCounter;
	
	public CompEnvironment(){
		this.previous=null;
		this.fields=new ArrayList<Field>();
		fieldCounter=0;
	}
	
	public CompEnvironment(CompEnvironment up){
		this();
		this.previous=up;
		fieldCounter=0;
	}

	public CompEnvironment beginScope() {
		return new CompEnvironment(this);
		
	}
	
	public CompEnvironment endScope() {
		return previous;
		
	}
	
	public FieldAssoc find(String id) throws UndeclaredIdentifierException{
		CompEnvironment current=this;
		int hops=0;
		while(current != null) {
			for(Field f:current.fields) {
				if(f.id().equals(id)) return new FieldAssoc(hops,f);
			}
			current=current.previous;
			hops++;
		}
		throw new UndeclaredIdentifierException(id);
		
	}
	
	public void assoc(String id,String type) throws ExistentIdentifierException {
		for(Field f: fields)
			if(f.id().equals(id))
				throw new ExistentIdentifierException(id);
		fields.add(new Field(id,"loc_" + (fieldCounter++),type));
	}


}
