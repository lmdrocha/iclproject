package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

public class StackFrame {
    private List<Field> fields;
    private int fieldCounter;
	private StackFrame previousFrame;
    private String frameName;

    public StackFrame(StackFrame parent, int frameID) {
        fields = new LinkedList<>();
        fieldCounter = 0;
        this.previousFrame = parent;
        frameName = "frame_" + frameID;
    }

    public Field associateField(String id, String type) {
        String name = "loc_" + (fieldCounter++);
        Field field =  new Field(id, name, type);
        fields.add(field);
        return field;
    }

    public String name() {
        return frameName;
    }

    public StackFrame parent() {
        return previousFrame;
    }

    private void writeFieldsIntro(PrintStream out) {
        out.println(".class " + frameName);
        out.println(".super java/lang/Object");
        if(previousFrame != null) {
            out.println(".field public sl L" + previousFrame.name() + ";");
        } else {
            out.println(".field public sl Ljava/lang/Object;");
        }
    }

    private void writeFields(PrintStream out) {
        for(Field field : fields) {
            out.println(".field public " + field.name() + " " + field.type());
        }
    }

    private void writeFieldsEpilogue(PrintStream out) {
        out.println(".method public <init>()V");
        out.println("    aload_0");
        out.println("    invokenonvirtual java/lang/Object/<init>()V");
        out.println("    return");
        out.println(".end method");
        out.println();
    }

    public void writeCode(String directory) throws FileNotFoundException {
        FileOutputStream fos = new FileOutputStream(directory + frameName + ".j");
        PrintStream out = new PrintStream(fos);

        writeFieldsIntro(out);
        writeFields(out);
        out.println();
        writeFieldsEpilogue(out);

        out.close();
    }
}
