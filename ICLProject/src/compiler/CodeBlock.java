package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

public class CodeBlock {
	private final String SL = "4";

	private String nameFile;

	private List<StackFrame> frames;
	private StackFrame frame;
	private int nrFrames;

	private List<String> code;

	public CodeBlock(String nameFile) {
		this.nameFile = nameFile;

		frames = new LinkedList<>();
		frame = null;
		nrFrames = 0;
		
		code = new LinkedList<>();
	}

	public void push(int val) {
		code.add("sipush " + val);
	}

	public void pop() {
		code.add("pop");
	}

	public void dup() {
		code.add("dup");
	}

	public void neg() {
		code.add("ineg");
	}

	public void add() {
		code.add("iadd");
	}

	public void sub() {
		code.add("isub");
	}

	public void mul() {
		code.add("imul");
	}

	public void div() {
		code.add("idiv");
	}
	
	public void frame_init() {
        StackFrame f = new StackFrame(frame, nrFrames++);
        frames.add(f);

        code.add("new " + f.name());
        code.add("dup");
        code.add("invokespecial " + f.name() + "/<init>()V");
        code.add("dup");
        code.add("aload " + SL);
        if(f.parent() != null) {
        	code.add("putfield " + f.name() + "/sl " + jvmType(f.parent().name()));
        } else {
        	code.add("putfield " + f.name() + "/sl " + jvmType(f.parent().name()));
        }
        code.add("astore " + SL);

        frame = f;
    }

    public void frame_end() {
        StackFrame parent = frame.parent();

        code.add("aload " + SL);
        if(parent != null) {
            get_field("sl", jvmType(parent.name()));
        } else {
            get_field("sl", jvmType("java/lang/Object"));
        }
        code.add("astore " + SL);

        frame = parent;
    }

    public void frame_assoc(String id, String type) {
        Field field = frame.associateField(id, type);
        put_field(field.name(), field.type());
    }

    public void frame_load() {
        code.add("aload " + SL);
    }

    public void get_frame_field(int hops, Field field) {
        StackFrame cur = frame;

        for(int h = 0; h < hops; h++) {
            StackFrame parent = cur.parent();
            get_field("sl", jvmType(parent.name()));
            cur = parent;
        }
        code.add("getfield " + cur.name() + "/" + field.name() + " " + field.type());
    }

    private void put_field(String name, String type) {
        code.add("putfield " + frame.name() + "/" + name + " " + type);
    }

    private void get_field(String name, String type) {
        code.add("getfield " + frame.name() + "/" + name + " " + type);
    }
    
    private String jvmType(String type) {
        return "L" + type + ";";
    }

	private void writeIntro(PrintStream out) {
		out.println(".class public Main");
		out.println(".super java/lang/Object");
		out.println();
		out.println(".method public <init>()V");
		out.println("    aload_0");
		out.println("    invokenonvirtual java/lang/Object/<init>()V");
		out.println("    return");
		out.println(".end method");
		out.println();
		out.println(".method public static main([Ljava/lang/String;)V");
		out.println("    .limit locals 10");
		out.println("    .limit stack 256");
		out.println("    getstatic java/lang/System/out Ljava/io/PrintStream;");
		out.println();
		out.println("    aconst_null");
		out.println("    astore " + SL);
	}

	private void writeCore(PrintStream out) {
		for (String str : code) {
			out.println("    " + str);
		}
	}

	private void writeEpilogue(PrintStream out) {
		out.println("    invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		out.println("    invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		out.println("    return");
		out.println(".end method");
		out.println();
	}

	public void writeCode(String directory) throws FileNotFoundException {
		for (StackFrame frame : frames) {
			frame.writeCode(directory);
		}

		FileOutputStream fOutputStream = new FileOutputStream(directory + this.nameFile+".j");
		PrintStream out = new PrintStream(fOutputStream);

		writeIntro(out);
		out.println();
		writeCore(out);
		out.println();
		writeEpilogue(out);

		out.close();
	}

}
