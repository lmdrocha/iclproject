package compiler;

public class FieldAssoc {

	private int hops;
	private Field field;
	
	public FieldAssoc(int hops, Field f) {
		this.hops=hops;
		field = f;
	}
	
	public int getHops() {
		return hops;
	}
	
	public Field getField() {
		return field;
	}

}
