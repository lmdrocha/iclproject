package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import objects.VReference;
import types.ASTRefType;
import types.IType;

public class ASTDerefer implements ASTNode {
	
	private ASTNode n;

	public ASTDerefer(ASTNode n) {
		this.n = n;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
        VReference refValue = (VReference) n.eval(env);
        IValue res = refValue.get();

        return res;
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) {
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		IType type = n.typecheck(env);
        ASTRefType refRes = new ASTRefType(type);

        return refRes;
	}

}
