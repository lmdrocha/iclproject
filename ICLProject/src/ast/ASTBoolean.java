package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import objects.VBoolean;
import types.ASTBoolType;
import types.IType;

public class ASTBoolean implements ASTNode{
	
	private boolean bool;

	public ASTBoolean(boolean bool) {
		this.bool = bool;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		return new VBoolean(bool);
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) {
		if(bool)
			code.push(1);
		else
			code.push(0);
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException {
		ASTBoolType boolRes;

        boolRes = ASTBoolType.singleton;

        return boolRes;
	}

}
