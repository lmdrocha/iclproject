package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import objects.VReference;
import types.ASTRefType;
import types.IType;

public class ASTRefer implements ASTNode {
	
	private ASTNode n;

	public ASTRefer(ASTNode n) {
		this.n = n;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
        IValue val = n.eval(env);
        VReference refRes = new VReference(val);

        return refRes;
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) {
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		IType type = n.typecheck(env);
        ASTRefType refRes = new ASTRefType(type);

        return refRes;
	}

}
