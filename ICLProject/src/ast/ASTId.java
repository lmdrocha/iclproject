package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import compiler.FieldAssoc;
import exceptions.ExistentIdentifierException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import types.IType;

public class ASTId implements ASTNode{
	
	private String id;
	public ASTId(String t1){
		id=t1;
		
	}
	@Override
	public IValue eval(Environment<IValue> env){
		return env.find(id);
	}
	@Override
	public void comp(CompEnvironment env, CodeBlock code) throws UndeclaredIdentifierException {
		code.frame_load();
		FieldAssoc fa=env.find(id);
		code.get_frame_field(fa.getHops(),fa.getField());
	}
	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException {
		 IType type = env.find(id);
	        if (type == null) {
	            throw new UndeclaredIdentifierException(id);
	        }
		    return type;
	}
}
