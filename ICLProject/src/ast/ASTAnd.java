package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.*;
import objects.Environment;
import objects.IValue;
import objects.VBoolean;
import types.ASTBoolType;
import types.IType;
public class ASTAnd implements ASTNode {

	private ASTNode left;
	private ASTNode right;

	public ASTAnd(ASTNode t1, ASTNode t2) {
		left = t1;
		right = t2;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		 VBoolean leftBool = (VBoolean) left.eval(env);
		 VBoolean rightBool = (VBoolean) right.eval(env);
		 VBoolean resBool = new VBoolean(leftBool.getValue() && rightBool.getValue());
		 return resBool;
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) throws ExistentIdentifierException, UndeclaredIdentifierException {
		
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		IType typeLeft = left.typecheck(env);
		IType typeRight = right.typecheck(env);

		if(typeLeft != ASTBoolType.singleton || typeRight != ASTBoolType.singleton) {
			throw new TypeErrorException("Illegal arguments to && operator");
		}

		ASTBoolType boolRes = ASTBoolType.singleton;
		return boolRes;
	}
	
}
