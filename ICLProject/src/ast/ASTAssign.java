package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import objects.VReference;
import types.ASTRefType;
import types.IType;

public class ASTAssign implements ASTNode {

	ASTNode ref, value;

	public ASTAssign(ASTNode ref, ASTNode value) {
		this.ref = ref;
		this.value = value;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		VReference leftVal = (VReference)ref.eval(env);
		IValue rightVal = value.eval(env);
		leftVal.setValue(rightVal);
		return leftVal;
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code)
			throws ExistentIdentifierException, UndeclaredIdentifierException {
		// TODO Auto-generated method stub

	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		IType type = ref.typecheck(env);

        if(!(type instanceof ASTRefType)) {
            throw new TypeErrorException("Illegal arguments to := operator");
        }

        ASTRefType refType = (ASTRefType) type;
        IType typeValue = value.typecheck(env);

        if(refType.subType() != typeValue) {
            throw new TypeErrorException("Illegal arguments to := operator");
        }

        return typeValue;
	}

}
