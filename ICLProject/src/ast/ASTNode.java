package ast;

import compiler.*;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import types.IType;

public interface ASTNode {
	
	IValue eval(Environment<IValue> env);
	
	IType typecheck(Environment<IType> env) throws TypeErrorException,UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException;
	
	void comp(CompEnvironment env, CodeBlock code) throws ExistentIdentifierException, UndeclaredIdentifierException;
	
}
