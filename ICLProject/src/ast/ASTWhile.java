package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import objects.VBoolean;
import types.ASTBoolType;
import types.IType;

public class ASTWhile implements ASTNode {

	ASTNode cond, exp;

	public ASTWhile(ASTNode condition, ASTNode expression) {
		this.cond = condition;
		this.exp = expression;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		VBoolean condBool = (VBoolean) cond.eval(env);
		while (condBool.getValue()) {
			exp.eval(env);
			condBool = (VBoolean) cond.eval(env);
		}
		return condBool;
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code)
			throws ExistentIdentifierException, UndeclaredIdentifierException {

	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		IType typeCond = cond.typecheck(env);

		if (typeCond != ASTBoolType.singleton) {
			throw new TypeErrorException("Illegal arguments to while operator");
		}

		ASTBoolType boolRes = ASTBoolType.singleton;
		return boolRes;
	}

}
