package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import objects.VInt;
import types.ASTIntType;
import types.IType;

public class ASTSub implements ASTNode {

	private ASTNode left;
	private ASTNode right;

	public ASTSub(ASTNode t1, ASTNode t2) {
		left = t1;
		right = t2;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		VInt v1 = (VInt)left.eval(env);
		VInt v2 = (VInt)right.eval(env);
		return new VInt(v1.getValue()-v2.getValue());
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) throws ExistentIdentifierException, UndeclaredIdentifierException {
		left.comp(env, code);
		right.comp(env, code);
		code.sub();
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		IType typeLeft = left.typecheck(env);
		IType typeRight = right.typecheck(env);

		if(typeLeft != ASTIntType.singleton || typeRight != ASTIntType.singleton) {
			throw new TypeErrorException("Illegal arguments to - operator");
		}

		ASTIntType intRes = ASTIntType.singleton;
		return intRes;
	}
	
	
	
}
