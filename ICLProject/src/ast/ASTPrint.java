package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import types.IType;

public class ASTPrint implements ASTNode {
	
	private ASTNode node;

	public ASTPrint(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		return node.eval(env);
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) {
		
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		return node.typecheck(env);
	}

}
