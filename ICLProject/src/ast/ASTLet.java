package ast;

import java.util.List;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Binding;
import objects.Environment;
import objects.IValue;
import types.IType;

public class ASTLet implements ASTNode{
	
	List<Binding> bindings;
	ASTNode t;

	public ASTLet(List<Binding> bindings, ASTNode t){
		this.bindings=bindings;
		this.t=t;	
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		Environment<IValue> e = env.beginScope();
		for(Binding bind: bindings){
			IValue value = bind.getExpression().eval(env);
			e.assoc(bind.getID(), value);		
		}
		IValue val = t.eval(e);
		e.endScope();
		return val;
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) throws ExistentIdentifierException, UndeclaredIdentifierException {
		code.frame_init();
		CompEnvironment e = env.beginScope();
        for(Binding bind: bindings) {
        	bind.getExpression().comp(e, code);
            e.assoc(bind.getID(),"I");
            code.frame_assoc(bind.getID(), "I");
        }
        t.comp(env,code);
        e.endScope();
        code.frame_end();
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		Environment<IType> e = env.beginScope();
        for(Binding bind: bindings){
			IType value = bind.getExpression().typecheck(env);
			
			if(!value.equals(bind.getType())) {
                throw new TypeErrorException("Illegal arguments to let operator");
            }

			e.assoc(bind.getID(), value);		
		}

        IType typeRes = t.typecheck(e);
        e.endScope();
        return typeRes;
	}

}
