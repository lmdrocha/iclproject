package ast;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.*;
import objects.Environment;
import objects.IValue;
import objects.IdType;
import objects.VFun;
import types.ASTFunType;
import types.IType;
public class ASTFunction implements ASTNode {

	private List<IdType> typelist;
    private ASTNode body;

	public ASTFunction(List<IdType> typelist, ASTNode body) {
		this.typelist = typelist;
		this.body=body;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		return new VFun(typelist, body, env);
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) throws ExistentIdentifierException, UndeclaredIdentifierException {
		
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {

        Environment<IType> newEnv = env.beginScope();
        Iterator<IdType>it = typelist.iterator();
        List<IType>typeArgs = new LinkedList<>();

        while(it.hasNext()) {
        	IdType tid = it.next();
            newEnv.assoc(tid.id(), tid.type());
            typeArgs.add(tid.type());
        }

        IType typeRet = body.typecheck(newEnv);
        ASTFunType funRes = new ASTFunType(typeArgs, typeRet);

        return funRes;
	}
	
}
