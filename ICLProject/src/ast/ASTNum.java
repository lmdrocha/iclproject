package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import objects.VInt;
import types.ASTIntType;
import types.IType;

public class ASTNum implements ASTNode {
	
	private int num;

	public ASTNum(int parseInt) {
		num = parseInt;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		return new VInt(num);
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) {
		code.push(num);
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException {
		return ASTIntType.singleton;
	}

}
