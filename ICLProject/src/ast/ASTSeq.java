package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import types.IType;

public class ASTSeq implements ASTNode{
	
	private ASTNode left, right;
	public ASTSeq(ASTNode t1, ASTNode t2){
		left=t1;
		right=t2;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		left.eval(env);
		IValue r=right.eval(env);
		return r;
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) throws ExistentIdentifierException, UndeclaredIdentifierException {
		left.comp(env, code);
		code.pop();
		right.comp(env, code);
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		 left.typecheck(env);
		 IType typeRes = right.typecheck(env);
	     return typeRes;
	}
	

}
