package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import objects.VBoolean;
import types.ASTBoolType;
import types.IType;

public class ASTNot implements ASTNode {
	private ASTNode value;
	
	public ASTNot (ASTNode val) {
		this.value = val;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		VBoolean nValue = (VBoolean) value.eval(env);

		VBoolean res = new VBoolean(!nValue.getValue());
		return res;
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) throws ExistentIdentifierException, UndeclaredIdentifierException {		
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		IType type = value.typecheck(env);

        if(type != ASTBoolType.singleton) {
            throw new TypeErrorException("Illegal arguments to ~ operator");
        }

        ASTBoolType boolRes = ASTBoolType.singleton;

        return boolRes;
	}
	
	
	
	
}
