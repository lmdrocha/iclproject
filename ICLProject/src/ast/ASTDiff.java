package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import objects.VBoolean;
import objects.VInt;
import types.ASTIntType;
import types.IType;

public class ASTDiff implements ASTNode {

	private ASTNode e1, e2;

	public ASTDiff(ASTNode e1, ASTNode e2) {
		this.e1 = e1;
		this.e2 = e2;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
        VInt intLeft	= (VInt) e1.eval(env);
        VInt intRight = (VInt) e2.eval(env);
        VBoolean boolRes = new VBoolean(intLeft.getValue() != intRight.getValue());

        return boolRes;
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) throws ExistentIdentifierException, UndeclaredIdentifierException {
		e1.comp(env, code);
		e2.comp(env, code);
		//Ver o que fazer!!!
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		IType typeLeft = e1.typecheck(env);
		IType typeRight = e2.typecheck(env);

		if(typeLeft != ASTIntType.singleton || typeRight != ASTIntType.singleton) {
			throw new TypeErrorException("Illegal arguments to != operator");
		}

		ASTIntType intRes = ASTIntType.singleton;
		return intRes;
	}

}
