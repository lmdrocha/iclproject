package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import objects.VInt;
import types.ASTIntType;
import types.IType;

public class ASTNeg implements ASTNode {
	private ASTNode value;
	
	public ASTNeg (ASTNode val) {
		this.value = val;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		VInt nValue = (VInt)value.eval(env);
		return new VInt(-nValue.getValue());
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) throws ExistentIdentifierException, UndeclaredIdentifierException {
		value.comp(env, code);
		code.neg();
		
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
        IType type = value.typecheck(env);

        if(type != ASTIntType.singleton) {
            throw new TypeErrorException("Illegal arguments to - operator");
        }

        ASTIntType intRes = ASTIntType.singleton;

        return intRes;
	}
	
	
	
	
}
