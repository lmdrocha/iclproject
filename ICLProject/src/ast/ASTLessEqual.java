package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.*;
import objects.Environment;
import objects.IValue;
import objects.VBoolean;
import objects.VInt;
import types.ASTBoolType;
import types.ASTIntType;
import types.IType;
public class ASTLessEqual implements ASTNode {

	private ASTNode left;
	private ASTNode right;

	public ASTLessEqual(ASTNode t1, ASTNode t2) {
		left = t1;
		right = t2;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		VInt leftBool = (VInt) left.eval(env);
		VInt rightBool = (VInt) right.eval(env);
		 VBoolean resBool = new VBoolean(leftBool.getValue() <= rightBool.getValue());
		 return resBool;
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) throws ExistentIdentifierException, UndeclaredIdentifierException {
		
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		IType typeLeft = left.typecheck(env);
		IType typeRight = right.typecheck(env);

		if(typeLeft != ASTIntType.singleton || typeRight != ASTIntType.singleton) {
			throw new TypeErrorException("Illegal arguments to <= operator");
		}

		ASTBoolType boolRes = ASTBoolType.singleton;
		return boolRes;
	}
	
}
