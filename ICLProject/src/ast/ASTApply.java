package ast;

import java.util.Iterator;
import java.util.List;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.ExistentIdentifierException;
import exceptions.NumberOfArgsInvalidException;
import exceptions.TypeErrorException;
import exceptions.UndeclaredIdentifierException;
import objects.Environment;
import objects.IValue;
import objects.IdType;
import objects.VFun;
import types.ASTFunType;
import types.IType;

public class ASTApply implements ASTNode{
	
	private ASTNode fun;
    private List<ASTNode> args;
	
	public ASTApply(ASTNode fun, List<ASTNode> args){
		this.fun=fun;
		this.args=args;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		VFun func = (VFun) fun.eval(env);

		Environment eFun = func.env();
		Environment eNew = eFun.beginScope();

		Iterator<IdType> iteratorIds = func.typelist().iterator();
		Iterator<ASTNode> iteratorArgs = args.iterator();

        while(iteratorIds.hasNext() && iteratorArgs.hasNext()) {
            String idArg = iteratorIds.next().id();
            IValue valArg = iteratorArgs.next().eval(env);
            eNew.assoc(idArg, valArg);
        }

        IValue result = func.body().eval(eNew);

        return result;
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code) throws ExistentIdentifierException, UndeclaredIdentifierException {
	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		IType type = fun.typecheck(env);

        if(!(type instanceof ASTFunType)) {
            throw new TypeErrorException("Illegal arguments to apply operator");
        }

        ASTFunType typeF = (ASTFunType) type;

        if(typeF.argumentsTypes().size() != args.size()) {
            throw new NumberOfArgsInvalidException("Illegal number of arguments");
        }

        Iterator<IType> iteratorTypes = typeF.argumentsTypes().iterator();
        Iterator<ASTNode> iteratorArgs = args.iterator();

        while(iteratorTypes.hasNext() && iteratorArgs.hasNext()) {
            IType typeFunctionArg = iteratorTypes.next();
            IType typeArg = iteratorArgs.next().typecheck(env);

            if(!typeFunctionArg.equals(typeArg)) {
                throw new TypeErrorException("Illegal arguments to apply operator");
            }
        }

        IType typeResult = typeF.resultType();

        return typeResult;
	}
	

}
