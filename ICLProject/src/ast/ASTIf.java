package ast;

import compiler.CodeBlock;
import compiler.CompEnvironment;
import exceptions.*;
import objects.Environment;
import objects.IValue;
import objects.VBoolean;
import types.ASTBoolType;
import types.IType;

public class ASTIf implements ASTNode {

	private ASTNode cond;
	private ASTNode ifBody;
	private ASTNode elseBody;

	public ASTIf(ASTNode cond, ASTNode ifBody, ASTNode elseBody) {
		this.cond = cond;
		this.ifBody = ifBody;
		this.elseBody = elseBody;
	}

	@Override
	public IValue eval(Environment<IValue> env) {
		VBoolean condBool = (VBoolean) cond.eval(env);
		IValue res;
		if (condBool.getValue()) {
			res = ifBody.eval(env);
		} else {
			res = elseBody.eval(env);
		}
		return res;
	}

	@Override
	public void comp(CompEnvironment env, CodeBlock code)
			throws ExistentIdentifierException, UndeclaredIdentifierException {

	}

	@Override
	public IType typecheck(Environment<IType> env)
			throws TypeErrorException, UndeclaredIdentifierException, ExistentIdentifierException, NumberOfArgsInvalidException {
		IType typeCond = cond.typecheck(env);
		IType typeIfBody = ifBody.typecheck(env);
		IType typeElseBody = elseBody.typecheck(env);

        if(typeCond != ASTBoolType.singleton) {
            throw new TypeErrorException("Illegal arguments to if cond");
        }
        if(typeIfBody != typeElseBody) {
            throw new TypeErrorException("Illegal arguments to if body");
        }

        return typeIfBody;
	}

}
